// @flow
/* global HTMLButtonElement HTMLHeadingElement */

import * as d3 from 'd3'
import header from './components/header/header.js'
import kombuchaFun from './components/kombuchaFun.js'
import kombuchaBottle from './components/kombucha-bottle/kombuchaBottle.js'

// // Create HTML elements in div
// const createKombucha = (): HTMLDivElement => {
//   const element: HTMLDivElement = document.createElement('div')

//   // Create Heading
//   const headerText: string = 'Kombucha Fun!'
//   const header: HTMLHeadingElement = document.createElement('h1')
//   header.className = 'header'
//   header.innerText = headerText
//   element.appendChild(header)

//   element.appendChild(button)

//   return element
// }

const createDiv = (elements: HTMLElement[]): HTMLDivElement => {
  const divElement: HTMLDivElement = document.createElement('div')
  elements.forEach(element => divElement.appendChild(element))
  return divElement
}

// Flow-safe DOM operation to add elements to DOM
if (document.body !== null) {
  document.body.appendChild(createDiv([header, kombuchaFun, kombuchaBottle]))
} else {
  throw new Error('document.body was null')
}

// D3 to demonstrate that packages are loaded
const svg = d3.select('body').append('svg')

svg
  .append('circle')
  .style('fill', 'steelblue')
  .attr('r', 30)
