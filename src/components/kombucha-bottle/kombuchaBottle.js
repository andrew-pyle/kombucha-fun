// @flow

import Icon from './milk-bottle.svg'

const kombuchaBottle: HTMLImageElement = new Image()
kombuchaBottle.src = Icon

export default kombuchaBottle
