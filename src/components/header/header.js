// @flow

import './style.css'

// Create Heading
const headerText: string = 'Kombucha Fun!'
const header: HTMLHeadingElement = document.createElement('h1')
header.className = 'header'
header.innerText = headerText

export default header
