// @flow

import getRandInt from './getRandInt'

// Create Button with Infinite Emojiis
const kombuchaFunButton: HTMLElement = document.createElement('button')
const kombuchaText: Array<string> = [
  'kombucha',
  'kombucha!',
  'KOMBUCHA',
  'KOMBUCHA!'
]
const kombuchaEmojii: Array<string> = ['🍶', '🍾', '🥛', '🥃', '🍵']
kombuchaFunButton.innerText = 'Click Me'
kombuchaFunButton.addEventListener('click', () => {
  kombuchaFunButton.innerText =
    kombuchaText[getRandInt(0, kombuchaText.length - 1)]
  kombuchaFunButton.insertAdjacentText(
    'afterend',
    kombuchaEmojii[getRandInt(0, kombuchaEmojii.length - 1)]
  )
})

export default kombuchaFunButton
