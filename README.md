# Kombucha Fun

Fun interactions with virtual Kombucha.

## Digital & Interactive Recipe

The vision for this project will be an interactive recipe for Kombucha, where the user will drag the ingradients into an empty jar, and end up with tasty kombucha!

I'm thinking a D3 dragging canvas with animations for each ingredient, and funny error animations for failing ingredient addition order.
