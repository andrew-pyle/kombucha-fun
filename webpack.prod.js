const config = require('./webpack.config')

module.exports = Object.assign({}, config, {
  // Change to 'cheap-eval-source-map' if too slow
  // See: https://webpack.js.org/configuration/devtool/
  mode: 'production'
})
